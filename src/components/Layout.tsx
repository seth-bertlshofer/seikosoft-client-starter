import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import moment from 'moment';
import NavMenu from './molecules/NavMenu';
import SideNav from './molecules/SideNav/SideNav';
import AlertBox from './atoms/AlertBox';
import ToastNotifications from './molecules/ToastNotifications/ToastNotifications';
import env from '../utils/env';

import { SubTitle, Contents } from '../styles';
import { LIGHTER_GREY } from '../styles/colors';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AlertBoxType from '../@types/alertBox';
import { NavRouteModel } from '../@types/ui/navRouteModel';

const Layout = ({
  alertBox,
  routes,
  windowWidth,
  windowHeight,
  showAlertBanner,
  user,
  login,
  logout,
  hideAlertBox,
  children,
}: LayoutProps) => {
  const today = parseInt(moment().format('DD'));

  let color;
  let stageColor;
  if (today % 5 === 0) {
    stageColor = 'rgba(193,66,66,0.5)';
  } else if (today % 3 === 0) {
    stageColor = 'rgba(246,13,246,0.5)';
  } else if (today % 2 === 0) {
    stageColor = 'rgba(246,137,13,0.5)';
  } else {
    stageColor = 'rgba(141,13,246,0.5)';
  }

  switch(env('REACT_APP_ENV')) {
    case 'development':
      color = 'rgba(193,66,66,0.5)';
      break;
    case 'staging':
      color = stageColor;
      break;
    default:
      color = LIGHTER_GREY;
      break;
  }

  const _hideAlertBox = () => hideAlertBox();
  const _removeFromQueue = id => console.log(id);
  const _hideFromQueue = id => console.log(id);

  return (
    <Container color={color} >
      <NavMenu
        showAlertBanner={showAlertBanner}
        user={user}
        login={login}
        logout={logout}
      />
      <SideNav
        routes={routes}
        windowWidth={windowWidth}
        windowHeight={windowHeight}
      />
      {children}
      {alertBox && (
        <AlertBox
          title={alertBox.title}
          actions={alertBox.actions}
          isOpen={!!alertBox}
          disableBackdropClick={alertBox.disableBackdropClick}
          onBackdropClick={_hideAlertBox}
          fullScreen={alertBox.fullScreen}
        >
          <div>
            {alertBox.reason && <SubTitle>
              Reason: {alertBox.reason}
            </SubTitle>}
            {alertBox.message && <Contents>
              {_.includes(alertBox.message, '\n') ? (
                _.map(alertBox.message.split('\n'), (line, key) => (
                  <div key={key}>{line}</div>
                ))
              ) : (
                alertBox.message
              )}
            </Contents>}
          </div>
        </AlertBox>
      )}
      <ToastNotifications
        removeFromQueue={_removeFromQueue}
        hideFromQueue={_hideFromQueue}
      />
      <ToastContainer />
    </Container>
  );
};

const Container = styled.div<{ color: string; }> `
  overflow: hidden;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: ${props => props.color};
  z-index: 0;
`;

interface LayoutProps {
  routes: NavRouteModel;
  windowWidth: number;
  windowHeight: number;
  alertBox?: AlertBoxType;
  showAlertBanner?: boolean;
  user?: any;
  login: () => void;
  logout: () => void;
  hideAlertBox: () => void;
  children: any;
}

export default Layout;