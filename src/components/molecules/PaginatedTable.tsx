import React from 'react';
import styled from 'styled-components';
import { TablePagination, TableFooter, TableRow } from '@material-ui/core';
import NormalTable, { ColumnData } from './NormalTable';

const PaginatedTable = ({
  page,
  rowsPerPage,
  totalRows,
  extraHeaders,
  sortBy,
  sortDirection,
  windowHeight,
  headers,
  rowData,
  onRowClick,
  handleChangePage,
  handleSorting,
}: PaginatedTableProps) => {
  const height = windowHeight ?? window.innerHeight;

  const _handleChangePage = (event: any, newPage: number) => handleChangePage(newPage);

  return (
    <TableSection>
      <NormalTable
        height={height - 280}
        extraHeaders={extraHeaders}
        sortBy={sortBy}
        sortDirection={sortDirection}
        headers={headers}
        rowData={rowData}
        onRowClick={onRowClick}
        handleSorting={handleSorting}
      />
      <br/>
      <Pagination>
        <TableFooter>
          <TableRow>
            <TablePagination
              colSpan={3}
              count={totalRows}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                'aria-label': 'previous page',
              }}
              nextIconButtonProps={{
                'aria-label': 'next page',
              }}
              onChangePage={_handleChangePage}
              labelRowsPerPage=""
              SelectProps={{ style: { display: 'none' } }}
            />
          </TableRow>
        </TableFooter>
      </Pagination>
    </TableSection>
  );
};

const TableSection = styled.div `
  position: relative;
`;
const Pagination = styled.table `
  position: absolute;
  bottom: -22px;
  right: 0;
`;

interface PaginatedTableProps {
  page: number;
  rowsPerPage: number;
  totalRows: number;
  extraHeaders?: any;
  sortBy?: string;
  sortDirection?: 'asc' | 'desc';
  windowHeight?: number;
  headers: ColumnData[];
  rowData: any;
  onRowClick?: (row: any) => any;
  handleChangePage: (newPage: number) => void;
  handleSorting?: (sorting: any) => any;
}

export default PaginatedTable;