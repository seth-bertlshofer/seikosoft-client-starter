import styled from 'styled-components';
import { CardContent } from '@material-ui/core';

export const NotificationsContainer = styled.div<{ hasNotifications?: boolean }> `
  position: absolute;
  z-index: ${props => props.hasNotifications ? '999' : '-1'};
  top: 65px;
  right: 0;
  width: 600px;
  text-align: center;
  display: flex;
  flex-direction: column;
  background-color: transparent;
  overflow-y: auto;
`;
export const CardSection = styled.div `
  margin: 5px;
`;
export const Content = styled(CardContent) `
  margin-top: -30px;
`;
export const Title = styled.h5 `
  display: inline-flex;
  margin: 0 auto;
`;