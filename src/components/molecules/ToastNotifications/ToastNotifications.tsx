import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { Grow, Card, CardHeader, CardActions }  from '@material-ui/core';
import Button from '../../atoms/Button';

import { NotificationsContainer, CardSection, Content, Title } from './styles';
import { BLUE } from '../../../styles/colors';

const ToastNotifications = ({ notifications, removeFromQueue, hideFromQueue }: ToastNotificationsProps) => {
  const [timers, setTimers] = useState<any[]>([]);

  useEffect(() => {
    _.each(timers, timer => window.clearTimeout(timer));
  });

  const hasNotifications = _.size(notifications) > 0;

  return (
    <NotificationsContainer hasNotifications={hasNotifications} >
      {hasNotifications && _.map(notifications, (notification, id) => {
        if (!_.isNil(notification.timeout)) {
          const timer = window.setTimeout(removeFromQueue(id), );
          setTimers([...timers, timer]);
        }

        return (
          <CardSection key={id}>
            <Grow
              in={notification.isShowing}
              style={{ transformOrigin: '0 0 0' }}
              {...(notification.isShowing ? { timeout: 1000 } : {})}
              onExited={() => removeFromQueue(id)}
            >
              <Card>
                <CardHeader title={<Title>{notification.title}</Title>} />
                <Content>{notification.message}</Content>
                <CardActions>
                  <Button
                    buttonType="flat"
                    variant="flat"
                    label="ok"
                    onClick={() => hideFromQueue(id)}
                    style={{ color: BLUE }}
                  />
                </CardActions>
              </Card>
            </Grow>
          </CardSection>
        );
      })}
    </NotificationsContainer>
  );
};

interface ToastNotificationsProps {
  notifications?: any;
  removeFromQueue: (id: string) => any;
  hideFromQueue: (id: string) => any;
}

export default ToastNotifications;