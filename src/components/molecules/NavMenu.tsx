import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import LoginMenu from './LoginMenu';
import { WHITE, BLUE, RED } from '../../styles/colors';
import ssLogo from '../../assets/seikosoft_logo/SeikosoftIcon.png';
import env from '../../utils/env';

const NavMenu = ({
  showAlertBanner,
  user,
  login,
  logout,
}: NavMenuProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <TitleBar position="fixed">
        <MyToolbar>
          <BrandLogo>
            <Logo to="/">
              <SsLogo src={ssLogo} alt="Seikosoft" />
            </Logo>
            <Title>
              <Typography variant="h6" >
                Seikosoft Starter
              </Typography>
              <SubTitle href={env('REACT_APP_VERSION_URL')} target="_blank" rel="noopener noreferrer">
                v{env('REACT_APP_VERSION')}
              </SubTitle>
            </Title>
          </BrandLogo>
          {showAlertBanner && <AlertSection><div>This is an unsupported browser</div></AlertSection>}
          <ButtonSection>
            <LoginMenu
              user={user}
              login={login}
              logout={logout}
            />
          </ButtonSection>
        </MyToolbar>
      </TitleBar>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      position: 'absolute',
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

const TitleBar = styled(AppBar) `
  background-color: ${BLUE};
  color: ${WHITE};
`;
const MyToolbar = styled(Toolbar) `
  position: relative;
`;
const BrandLogo = styled.div `
  display: inline-flex;
  position: relative;
`;
const Logo = styled(Link) `
`;
const SsLogo = styled.img `
  height: 55px;
  width: auto;
`;
const ButtonSection = styled.div `
  position: absolute;
  right: 0;
`;
const Title = styled.div `
  position: relative;
  color: ${WHITE};
  font-weight: Bold;
  font-size: 24px;
  margin: 5px;
  line-height: 1;
  margin: 20px 0 0 5px;
`;
const SubTitle = styled.a `
  cursor: pointer;
  position: absolute;
  color: ${WHITE};
  top: 25px;
  right: 0;
  font-size: 10px;
  margin: 2px auto;
`;
const AlertSection = styled.div `
  background-color: ${RED};
  color: ${WHITE};
  text-align: center;
  display: inline-flex;
  position: relative;
  margin: 0 auto;
  text-align: center;
  width: 400px;

  > div {
    width: 400px;
  }
`;

interface NavMenuProps {
  showAlertBanner?: boolean;
  user?: any;
  login: () => void;
  logout: () => void;
}

export default NavMenu;
