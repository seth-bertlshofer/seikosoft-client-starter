import styled from 'styled-components';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { GREY, WHITE, BLACK, BLUE } from '../../../styles/colors';

export const CloseButton = styled.div `
  position: absolute;
  right: 5px;
  top: 5px;
  color: ${ GREY };
`;
export const DrawSection = styled.div<{ height: number }> `
  position: absolute;
  height: ${props => props.height - 70}px;
`;
export const SeikosoftIcon = styled.img `
  height: 25px;
  width: auto;
`;
export const SeikosoftLogo = styled.img `
  height: auto;
  width: 100px;
`;
export const MyListItem = styled(ListItem) `
  max-height: 40px;
  color: ${props => props.selected ? WHITE : BLACK};
  background-color: ${props => props.selected ? BLUE : WHITE};
`;
export const MenuIcon = styled(ListItemIcon) `
`;
export const MenuText = styled(ListItemText) `
  white-space: break-spaces;
`;
