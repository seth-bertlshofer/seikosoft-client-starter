import React from 'react';
import {
  LocalShipping,
  AttachMoney,
  MonetizationOn,
  Class,
  Poll,
  Email,
  Group,
  PeopleOutline,
  List,
  AddAlert,
  MoneyOff,
  Description,
  BlurOn,
}  from '@material-ui/icons';

const SideNaveIcons = (icon: string) => {
  switch(icon) {
    case 'local_shipping':
      return <LocalShipping />;
    case 'attach_money':
      return <AttachMoney />;
    case 'monetization_on':
      return <MonetizationOn />;
    case 'class':
      return <Class />;
    case 'poll':
      return <Poll />;
    case 'email':
      return <Email />;
    case 'group':
      return <Group />;
    case 'people_outline':
      return <PeopleOutline />;
    case 'list':
      return <List />;
    case 'add_alert':
      return <AddAlert />;
    case 'money_off':
      return <MoneyOff />;
    case 'document':
      return <Description />;
    case 'blur_on':
      return <BlurOn />;
    default:
      return <div />;
  }
};

export default SideNaveIcons;