import React, { useState, useCallback, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import clsx from 'clsx';
import _    from 'lodash';
import {
  IconButton,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Collapse,
  CssBaseline,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import LinkIcon from '@material-ui/icons/Link';
import { ExpandMore, ExpandLess } from '@material-ui/icons';

import seikosoftLogo from '../../../assets/seikosoft_logo/PoweredBySeikosoft_cropped.png';
import seikosoftIcon from '../../../assets/seikosoft_logo/SeikosoftIcon.png';
import sideNavIcon from './SideNavIcons';
import env from '../../../utils/env';

import { DrawSection, SeikosoftIcon, SeikosoftLogo, MyListItem, MenuIcon, MenuText } from './styles';
import { WHITE, BLACK, BLUE, GREY } from '../../../styles/colors';

import { ExternalLinkModel, NavRouteModel } from '../../../@types/ui/navRouteModel';
import { SubRouteModel } from '../../../@types/ui/subRouteModel';

const drawerWidth = 250;

const SideNav = ({ windowWidth, windowHeight, routes, externalLinks }: SideNavProps) => {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();

  const [sideNavOpen, setSideNavOpen] = useState(windowWidth > 1080);
  const [activeSubMenu, setActiveSubMenu] = useState('');

  const _autoSideNav = useCallback(() => {
    setSideNavOpen(windowWidth > 1080);
  }, [windowWidth]);

  const _openSideNav = () => setSideNavOpen(true);
  const _closeSideNav = () => setSideNavOpen(false);
  const _openSubNav = subRoute => setActiveSubMenu(subRoute);
  const _closeSubNav = () => setActiveSubMenu('');

  const _routeTo = (route: string) => () => history.push(route);
  const _externalLink = (link?: string) => () => window.open(link, '_blank', 'noopener noreferrer');

  useEffect(() => {
    _autoSideNav();
  }, [_autoSideNav]);

  if (!routes) {
    return (<div></div>);
  } else {
    return (
      <div>
        <CssBaseline />
        <DrawSection height={windowHeight}>
          <Drawer
            variant="permanent"
            PaperProps={{ style: { height: `${windowHeight - 65}px` }}}
            className={clsx({
              [classes.drawerOpen]: sideNavOpen,
              [classes.drawerClose]: !sideNavOpen,
            })}
            classes={{
              paper: clsx({
                [classes.drawerOpen]: sideNavOpen,
                [classes.drawerClose]: !sideNavOpen,
              }),
            }}
          >
            <div
              className={classes.toolbar}
              style={{ height: '20px', minHeight: '20px' }}
            >
              <IconButton onClick={sideNavOpen ? _closeSideNav : _openSideNav}>
                {sideNavOpen ? <ChevronLeftIcon /> : <ChevronRightIcon />}
              </IconButton>
            </div>
            <Divider />
            <List>
              {_.map<any>(routes, (route: NavRouteModel, idx: number) => {
                if (route.SubRoutes) {
                  const selected = _.includes(location.pathname, route.Route);
                  const open = route.Label === activeSubMenu || selected;
                  const style = {
                    color: open ? WHITE : BLACK,
                    backgroundColor: open ? BLUE : WHITE,
                  };

                  return (
                    <div key={`main-${route.Label}-${idx}`}>
                      <MyListItem
                        button
                        title={route.Label}
                        onClick={() => open ? _closeSubNav() : _openSubNav(route.Label)}
                        selected={selected}
                        style={style}
                      >
                        <MenuIcon style={style}>{sideNavIcon(route.Icon)}</MenuIcon>
                        <MenuText primary={route.Label} style={style}/>
                        {open ? <ExpandLess /> : <ExpandMore />}
                      </MyListItem>
                      <Collapse in={open} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                          {_.map(route.SubRoutes, (subRoute: SubRouteModel, idx: number) => {
                            const selected = _.includes(location.pathname, subRoute.Route);
                            const style = {
                              color: selected ? BLUE : BLACK,
                              backgroundColor: selected ? GREY : WHITE,
                            };
                            return (
                              <MyListItem
                                key={`sub-${subRoute.Label}=${idx}`}
                                button
                                title={subRoute.Label}
                                className={classes.nested}
                                selected={selected}
                                onClick={_routeTo(subRoute.Route)}
                                style={style}
                              >
                                <MenuText style={style} primary={subRoute.Label} />
                              </MyListItem>
                            );
                          })}
                        </List>
                      </Collapse>
                    </div>
                  );
                } else {
                  const selected = _.includes(location.pathname, route.Route);
                  const style = {
                    color: selected ? WHITE : BLACK,
                    backgroundColor: selected ? BLUE : WHITE,
                  };

                  return (
                    <MyListItem
                      key={`main-${route.Label}-${idx}`}
                      button
                      title={route.Label}
                      selected={selected}
                      onClick={_routeTo(route.Route)}
                      style={style}
                    >
                      <MenuIcon style={style}>{sideNavIcon(route.Icon)}</MenuIcon>
                      <MenuText style={style} primary={route.Label} />
                    </MyListItem>
                  );
                }
              })}
            </List>
            <Divider />
            <ListItem
              button
              title="https://www.seikosoft.com/"
              onClick={_externalLink(env('REACT_APP_SEIKOSOFT'))}
            >
              <ListItemIcon><SeikosoftIcon src={seikosoftIcon} alt="" /></ListItemIcon>
              <ListItemText primary={<SeikosoftLogo src={seikosoftLogo} alt="Seikosoft" />} />
            </ListItem>
            <Divider />
            <ListItem
              button
              onClick={() => activeSubMenu === 'external-links' ? _closeSubNav() : _openSubNav('external-links')}
              selected={false}
            >
              <ListItemIcon><LinkIcon /></ListItemIcon>
              <ListItemText primary="Links" />
              {activeSubMenu === 'external-links' ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={activeSubMenu === 'external-links'} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {_.map<any>(externalLinks, (link: ExternalLinkModel, key) => (
                  <ListItem
                    key={`external-link-${key}`}
                    button
                    title={link.title}
                    onClick={_externalLink(link.url)}
                  >
                    <ListItemText primary={link.title} />
                  </ListItem>
                ))}
              </List>
            </Collapse>
          </Drawer>
        </DrawSection>
      </div>
    );
  }
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawerOpen: {
      marginTop: '65px',
      width: drawerWidth,
      scrollbarWidth: 'thin',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      marginTop: '65px',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
      scrollbarWidth: 'thin',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
  }),
);

interface SideNavProps {
  windowWidth: number;
  windowHeight: number;
  routes: NavRouteModel;
  externalLinks?: ExternalLinkModel;
  isSpinning?: boolean;
}

export default SideNav;