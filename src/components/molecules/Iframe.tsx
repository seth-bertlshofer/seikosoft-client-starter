import React, { useEffect } from 'react';
import { FormControl, InputLabel, FormHelperText } from '@material-ui/core';

const Iframe = ({
  label,
  title,
  source,
  frameboarder,
  scrolling,
  width,
  height,
  inputStyle,
  errorMessage,
  callback,
}: IframeProps) => {
  useEffect(() => {
    window.addEventListener('message', e => callback(e.data), false);

    return () => {
      window.removeEventListener('message', e => callback(e.data), false);
    };
  }, [callback]);

  const hasError = errorMessage ? errorMessage !== '' : false;

  return (
    <FormControl
      error={hasError}
      margin="normal"
      style={inputStyle}
    >
      {label && <div style={{ marginBottom: '35px' }}><InputLabel htmlFor={title} style={{ marginLeft: '10px' }}>{label}</InputLabel></div>}
      <iframe
        id={title}
        title={title}
        src={source}
        frameBorder={frameboarder}
        scrolling={scrolling}
        width={width}
        height={height}
      ></iframe>
      {hasError && <FormHelperText error={hasError}>{errorMessage}</FormHelperText>}
    </FormControl>
  );
};
interface IframeProps {
  label?: string;
  title: string;
  source: string;
  frameboarder?: 0 | 1;
  scrolling?: 'yes' | 'no';
  width?: number;
  height?: number;
  inputStyle?: Record<string, unknown>;
  errorMessage?: string;
  callback: (token: string) => void;
}

export default Iframe;