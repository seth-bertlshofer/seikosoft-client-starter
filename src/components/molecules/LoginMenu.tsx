import React, { Fragment } from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import Button from '../atoms/Button';
import { WHITE } from '../../styles/colors';

const LoginMenu = ({ user, login, logout }: LoginMenuProps) => {

  const _logout = () => logout();
  const _login = async () => login();

  return (
    <Fragment>
      <ButtonContainer>
        {!user ? (
          <Button
            buttonType="flat"
            variant="flat"
            label="Login"
            onClick={_login}
            style={{ color: WHITE }}
          />
        ) : (
          <>
            <Button
              buttonType="flat"
              variant="flat"
              label="Logout"
              onClick={_logout}
              style={{ color: WHITE }}
            />
          </>
        )}
      </ButtonContainer>
    </Fragment>
  );
};

const ButtonContainer = styled.div `
  display: inline;
`;

interface LoginMenuProps {
  user?: any;
  login: () => void;
  logout: () => void;
}

export default LoginMenu;