import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import { TableContainer, TableRow, TableCell, Table, TableHead, TableBody, CircularProgress, TableSortLabel } from '@material-ui/core';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { BLUE } from '../../styles/colors';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }),
);

const NormalTable = ({
  extraHeaders,
  sortBy,
  sortDirection,
  headers,
  rowData,
  onRowClick,
  handleSorting,
  height,
  isSpinning,
}: NormalTableProps) => {
  const classes = useStyles();

  const _onRowClick = row => onRowClick ? onRowClick(row) : null;
  const _sort = dataKey => () => {
    const isAsc = sortBy === dataKey && sortDirection === 'asc';

    if (handleSorting) {
      handleSorting({
        sortBy: dataKey,
        sortDirection: isAsc ? 'desc' : 'asc'
      });
    }
  };

  return (
    <Container height={height}>
      <Table stickyHeader size="small">
        <TableHead>
          {extraHeaders}
          <TableRow>
            {_.map(headers, (header, idx) => (
              <Column key={`header-${idx}`} width={header.width} >
                {handleSorting && !header.sortDisabled ? (
                  <TableSortLabel
                    active={sortBy === header.dataKey}
                    direction={sortBy === header.dataKey ? sortDirection : 'asc'}
                    onClick={_sort(header.dataKey)}
                  >
                    {header.label}
                    {sortBy === header.dataKey ? (
                      <span className={classes.visuallyHidden}>
                        {sortDirection === 'desc' ? 'sorted descending' : 'sorted ascending'}
                      </span>
                    ) : null}
                  </TableSortLabel>
                ) : (
                  <div>{header.label}</div>
                )}
              </Column>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {_.size(rowData) > 0 ? _.map(rowData, (row, key) => (
            <TableRow
              key={`row-${key}`}
              tabIndex={-1}
              onClick={_onRowClick(row)}
              style={{ cursor: onRowClick ? 'pointer' : '' }}
              hover
            >
              {_.map(headers, (item, idx) => {
                const dataItem = row[item.dataKey];
                if (dataItem === null || dataItem === undefined || dataItem === '') return <Column key={`cell-${idx}`} width={item?.width ?? ''}>-</Column>;
                return <Column key={`cell-${idx}`} width={item.width}>{dataItem}</Column>;
              })}
            </TableRow>
          )) : (
            <TableRow>
              <TableCell>{isSpinning ? <CircularProgress size={24} thickness={3} style={{ color: BLUE }}/> : 'Currently no information to show'}</TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </Container>
  );
};

const Container = styled(TableContainer)<{ height?: number }> `
  max-height: ${props => (props.height || 400)}px;
`;
export const Column = styled(TableCell)<{ width: number }> `
  min-width: ${props => props.width || 100}px;
`;

export interface ColumnData {
  dataKey: string;
  label: string;
  numeric?: boolean;
  width: number;
  sortDisabled?: boolean
}

interface NormalTableProps {
  extraHeaders?: any;
  sortBy?: string;
  sortDirection?: 'asc' | 'desc';
  headers: ColumnData[];
  rowData: any;
  onRowClick?: (row: any) => any;
  handleSorting?: (sorting: any) => any;
  height?: number;
  isSpinning?: boolean;
}

export default NormalTable;