import React, { useState } from 'react';
import styled from 'styled-components';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import {
  Grid,
  List,
  Card,
  CardHeader,
  Divider,
  ListItem,
  ListItemText,
  ListItemIcon,
  Button,
  Checkbox,
  FormControl,
  FormHelperText,
} from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import _ from 'lodash';
import { GREEN } from '../../styles/colors';

const not = (a: any[], b: any[]) => _.filter(a, value => b.indexOf(value) === -1);
const intersection = (a: any[], b: any[]) => _.filter(a, value => b.indexOf(value) !== -1);

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '460px',
      margin: '0 -25px',
    },
    cardHeader: {
      padding: theme.spacing(1, 2),
    },
    list: {
      width: 170,
      height: 230,
      backgroundColor: theme.palette.background.paper,
      overflow: 'auto',
    },
    button: {
      margin: theme.spacing(0.5, 0),
    },
  }),
);

const TransferList = ({ name, labels, disabled, left, setLeft, right, setRight, errorMessage, hiddenVals }: TransferListProps) => {
  const classes = useStyles();
  const [checked, setChecked] = useState<any[]>([]);

  const leftChecked = intersection(checked, left);
  const rightChecked = intersection(checked, right);

  const _handleToggle = (value: any) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const _handleAllRight = () => {
    setRight(right.concat(left));
    setLeft([]);
  };

  const _handleCheckedRight = () => {
    setRight(right.concat(leftChecked));
    setLeft(not(left, leftChecked));
    setChecked(not(checked, leftChecked));
  };

  const _handleCheckedLeft = () => {
    setLeft(left.concat(rightChecked));
    setRight(not(right, rightChecked));
    setChecked(not(checked, rightChecked));
  };

  const _handleAllLeft = () => {
    setLeft(left.concat(right));
    setRight([]);
  };

  const customList = (title: string, items: any[]) => (
    <Card>
      <CardHeader
        className={classes.cardHeader}
        title={<Title>{title}</Title>}
      />
      <Divider />
      <List className={classes.list} dense component="div" role="list">
        {_.map(items, (value: any) => {
          const labelId = `transfer-list-item-${value}-label`;
          const label = labels(value);

          return (
            <ListItem key={`${name}-${value}`} role="listitem" button onClick={_handleToggle(value)}>
              <ListItemIcon>
                <Checkbox
                  checked={checked.indexOf(value) !== -1}
                  checkedIcon={<CheckIcon style={{ color: GREEN }} />}
                  tabIndex={-1}
                  disableRipple
                  disabled={disabled}
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={label} />
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Card>
  );

  const hasError = errorMessage ? errorMessage !== '' : false;

  return (
    <FormControl
      disabled={disabled}
      error={hasError}
      margin="normal"
    >
      <Grid container spacing={2} justify="center" alignItems="center" className={classes.root}>
        <Grid item>{customList('Available', not(left, hiddenVals ?? []))}</Grid>
        <Grid item>
          <Grid container direction="column" alignItems="center">
            <Button
              variant="outlined"
              size="small"
              className={classes.button}
              onClick={_handleAllRight}
              disabled={left.length === 0 || disabled}
              aria-label="move all right"
            >
              <LastPageIcon />
            </Button>
            <Button
              variant="outlined"
              size="small"
              className={classes.button}
              onClick={_handleCheckedRight}
              disabled={leftChecked.length === 0 || disabled}
              aria-label="move selected right"
            >
              <ChevronRightIcon />
            </Button>
            <Button
              variant="outlined"
              size="small"
              className={classes.button}
              onClick={_handleCheckedLeft}
              disabled={rightChecked.length === 0 || disabled}
              aria-label="move selected left"
            >
              <ChevronLeftIcon />
            </Button>
            <Button
              variant="outlined"
              size="small"
              className={classes.button}
              onClick={_handleAllLeft}
              disabled={right.length === 0 || disabled}
              aria-label="move all left"
            >
              <FirstPageIcon />
            </Button>
          </Grid>
        </Grid>
        <Grid item>{customList('Selected', not(right, hiddenVals ?? []))}</Grid>
      </Grid>
      {hasError && <FormHelperText error={hasError}>{errorMessage}</FormHelperText>}
    </FormControl>
  );
};

const Title = styled.div `
  font-size: 15px;
`;

interface TransferListProps {
  name: string;
  labels: (value: any) => string;
  disabled?: boolean;
  left: any[]
  setLeft: (left: any[]) => void;
  right: any[];
  setRight: (right: any[]) => void;
  errorMessage?: string;
  hiddenVals?: any[];
}

export default TransferList;