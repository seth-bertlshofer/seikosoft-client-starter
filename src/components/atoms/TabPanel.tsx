import React from 'react';
import { Typography, Box } from '@material-ui/core';

const TabPanel = ({ children, value, index, ...other }: TabPanelProps) => (
  <Typography
    component="div"
    role="tabpanel"
    hidden={value !== index}
    id={`vertical-tabpanel-${index}`}
    aria-labelledby={`vertical-tab-${index}`}
    {...other}
    style={{ width: '780px' }}
  >
    {value === index && <Box p={3}>{children}</Box>}
  </Typography>
);

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

export default TabPanel;