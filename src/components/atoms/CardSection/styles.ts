import styled from 'styled-components';
import { GREY, WHITE } from '../../../styles/colors';

export const Container = styled.div<{ width?: string; }> `
  width: ${props => props.width || ''};
`;
export const Title = styled.h5<{ color?: string }> `
  width: 100%;
  font-size: 15px;
  text-align: center;
  color: ${props => props.color || WHITE};
  background-color: ${props => props.color || GREY};
`;
export const ChildContainer = styled.div `
  margin: 5px 10px;
`;