import React, { ReactElement } from 'react';

import { Container, Title, ChildContainer }  from './styles';

const CardSection = ({ title, children, width }: CardSectionProps) => (
  <Container width={width}>
    <Title>{title}</Title>
    <ChildContainer>
      {children}
    </ChildContainer>
  </Container>
);

interface CardSectionProps {
  title: string | ReactElement;
  children: any;
  width?: string;
}

export default CardSection;