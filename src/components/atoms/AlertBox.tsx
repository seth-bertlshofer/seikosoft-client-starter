import React      from 'react';
import _          from 'lodash';
import styled     from 'styled-components';
import { Dialog, DialogActions, DialogContent, DialogTitle, IconButton } from '@material-ui/core';
import CloseIcon  from '@material-ui/icons/Close';
import Button     from './Button';
import { GREY }   from '../../styles/colors';
import { AlertBoxActions } from '../../@types/alertBox';

const AlertBox = ({ title, actions, isOpen, isSpinning, fullWidth, fullScreen, disableBackdropClick, onClose, onBackdropClick, children, styles, scroll }: AlertBoxProps) => (
  <Dialog
    open={isOpen}
    fullWidth={fullWidth}
    disableBackdropClick={disableBackdropClick}
    disableEscapeKeyDown={disableBackdropClick}
    onClose={onBackdropClick}
    fullScreen={fullScreen}
    style={styles}
    scroll={scroll}
  >
    <DialogTitle>
      {title}
      {onClose && (
        <CloseButton>
          <IconButton aria-label="close" onClick={onClose}>
            <CloseIcon />
          </IconButton>
        </CloseButton>
      )}
    </DialogTitle>
    <DialogContent>
      {children}
    </DialogContent>
    <DialogActions>
      {_.map(actions, (action, label) => (
        <Button
          key={`button-${label}`}
          buttonType="flat"
          variant="flat"
          label={label}
          style={{
            ...action.styles,
            color: !isSpinning ? action.color : GREY
          }}
          onClick={action.onClick}
          autoFocus={label === 'Cancel'}
          disabled={isSpinning}
        />
      ))}
    </DialogActions>
  </Dialog>
);

const CloseButton = styled.div `
  position: absolute;
  right: 5px;
  top: 5px;
  color: ${ GREY };
`;

interface AlertBoxProps {
  title: string;
  actions?: AlertBoxActions;
  isOpen: boolean;
  isSpinning?: boolean;
  fullWidth?: boolean;
  fullScreen?: boolean;
  scroll?: any;
  disableBackdropClick?: boolean;
  onClose?: () => void;
  onBackdropClick?: () => void;
  children: string | any;
  styles?: Record<string, unknown>;
}

export default AlertBox;