import React, { ReactElement } from 'react';
import { FormControl, FormHelperText, FormControlLabel, Switch, withStyles } from '@material-ui/core';
import { LIGHT_RED, BLUE } from '../../styles/colors';

const MySwitch = ({
  label,
  checked,
  activeColor,
  inactiveColor,
  inputProps,
  disabled,
  required,
  style,
  errorMessage,
  onChange,
}: SwitchProps) => {
  const _onChange = e => onChange ? onChange(e.target.checked) : {};

  const ColorSwitch = withStyles({
    switchBase: {
      color: inactiveColor ?? LIGHT_RED,
      '&$checked': {
        color: activeColor ?? BLUE,
      },
      '&$checked + $track': {
        backgroundColor: activeColor ?? BLUE,
      },
    },
    checked: {},
    track: {},
  })(Switch);

  const SwitchElement = (
    <ColorSwitch
      id={`${Math.random()}`}
      checked={checked}
      onChange={_onChange}
      inputProps={inputProps}
      required={required}
      disabled={disabled}
    />
  );

  const hasError = errorMessage ? errorMessage !== '' : false;

  return (
    <FormControl
      disabled={disabled}
      error={hasError}
      margin="normal"
      required={required}
      style={style}
    >
      {label ? (
        <FormControlLabel
          control={SwitchElement}
          label={label}
        />
      ) : SwitchElement}
      {hasError && <FormHelperText error={hasError}>{errorMessage}</FormHelperText>}
    </FormControl>
  );
};

interface SwitchProps {
  label?: string | ReactElement;
  checked: boolean;
  activeColor?: string;
  inactiveColor?: string;
  inputProps?: Record<string, unknown>;
  disabled?: boolean;
  required?: boolean;
  style?: Record<string, unknown>;
  errorMessage?: string;
  onChange: (value: boolean) => void;
}

export default MySwitch;