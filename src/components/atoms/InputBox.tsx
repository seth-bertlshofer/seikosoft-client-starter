import React, { ReactText } from 'react';
import { FormControl, FormHelperText, TextField }  from '@material-ui/core';

const InputBox = ({
  multiline,
  label,
  value,
  type,
  variant,
  inputProps,
  inputStyle,
  style,
  placeholder,
  rows,
  rowsMax,
  autoFocus,
  disabled,
  fullWidth,
  required,
  errorMessage,
  onChange,
  onBlur,
  onFocus,
  onPaste,
}: InputBoxProps) => {
  const _onChange = e => onChange ? onChange(e.target.value) : {};
  const _onBlur = e => onBlur ? onBlur(e.target.value) : {};
  const _onFocus = e => onFocus ? onFocus(e.target.value) : {};
  const _onPaste = e => onPaste ? onPaste(e) : {};

  const hasError = errorMessage ? errorMessage !== '' : false;

  return (
    <FormControl
      disabled={disabled}
      fullWidth={fullWidth}
      error={hasError}
      margin="normal"
      required={required}
      style={style}
    >
      <TextField
        id={`${Math.random()}`}
        label={label}
        type={type || 'text'}
        variant={variant || 'standard'}
        value={value}
        inputProps={inputProps}
        placeholder={placeholder}
        onChange={_onChange}
        onBlur={_onBlur}
        onFocus={_onFocus}
        onPaste={_onPaste}
        autoFocus={autoFocus}
        disabled={disabled}
        style={{ ...inputStyle, height: (inputStyle && inputStyle.height)}}
        multiline={multiline}
        rows={multiline && rows ? rows : ''}
        rowsMax={multiline && rowsMax ? rowsMax : ''}
        error={hasError}
      />
      {hasError && <FormHelperText error={hasError}>{errorMessage}</FormHelperText>}
    </FormControl>
  );
};

interface InputBoxProps {
  value?: string | number | undefined | null;
  onChange?: (value: string | number | ReactText) => void;
  multiline?: boolean;
  label?: string;
  type?: string;
  variant?: any; // standard | outlined | filled
  inputProps?: Record<string, unknown>;
  inputStyle?: any;
  style?: Record<string, unknown>;
  placeholder?: string;
  rows?: number;
  rowsMax?: number;
  onBlur?: (value: string | number) => void;
  onFocus?: (value: string | number) => void;
  onPaste?: (value: string | number) => void;
  autoFocus?: boolean;
  disabled?: boolean;
  fullWidth?: boolean;
  required?: boolean;
  errorMessage?: string;
}

export default InputBox;
