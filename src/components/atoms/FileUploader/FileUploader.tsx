// https://spin.atomicobject.com/2018/09/13/file-uploader-react-typescript/
import React, { ReactElement, useState, useEffect, useRef } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, CircularProgress } from '@material-ui/core';
import _  from 'lodash';

import Button from '../../atoms/Button';
import API  from '../../../utils/network';

import { Blue, Container, Contents, DropZone, ErrorMessage, FileName, MessageContainer } from './styles';
import { BLUE, GREY } from '../../../styles/colors';

const FileUploader = ({
  isOpen = false,
  isChild,
  subBucket,
  message,
  acceptFileTypes = ['image/*'],
  maxFileSizeKB = 50,
  callback,
  uploadFile,
  hideFileUploader,
  isPublic = false,
  isSpinning,
  startSpinner,
  stopSpinner,
  hideAlertBox,
  showErrorMessage,
}: FileUploaderProps) => {
  const dropzone = useRef(null);

  const [file, setFile] = useState<File>();
  const [signedUrl, setSignedUrl] = useState('');
  const [documentId, setDocumentId] = useState('');
  const [dragEventCounter, setDragEventCounter] = useState<number>(0);
  const [dragging, setDragging] = useState<boolean>(false);
  const [errors, setErrors] = useState<Error>( {
    fileSize: '',
    fileType: '',
  });
  const _hideFileUploader = () => {
    setErrors({
      fileSize: '',
      fileType: '',
    });
    hideFileUploader();
  };

  let measure = 'KB';
  let fileSize = maxFileSizeKB;
  if (maxFileSizeKB >= 1024) {
    measure = 'MB';
    fileSize = maxFileSizeKB / 1024;
  }

  const _overrideEventDefaults = (e: Event | React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const _getFileSizeInKB = (file: File) => Number((file.size/1024).toFixed(0));

  const _returnFileSize = (number: number): string => {
    if(number < 1024) {
      return `${number} bytes`;

    } else if(number >= 1024 && number < 1048576) {
      return `${(number/1024).toFixed(1)} KB`;

    } else if(number >= 1048576) {
      return `${(number/1048576).toFixed(1)} MB`;
    }
    return '0';
  };

  // ******************** functions ********************
  const _getSignedUrl = file =>
    API({
      method: 'POST',
      url: 's3/get-upload-url',
      data: {
        subBucket,
        fileName: file.name,
        contentType: file.type,
        isPublic,
      }
    })
      .then(response => {
        setFile(file);
        setSignedUrl(response.data.signedUrl);
        setDocumentId(response.data.documentId);
        callback && callback(file, response.data);
        stopSpinner && stopSpinner();
      })
      .catch(error =>
        showErrorMessage && showErrorMessage(error, { Ok: { onClick: () => {
          hideAlertBox && hideAlertBox();
          stopSpinner && stopSpinner();
        } }}));

  const _validate = (file: File): boolean => {
    const newErrors = {
      fileSize: '',
      fileType: '',
    };

    if (_getFileSizeInKB(file) >= maxFileSizeKB) {
      newErrors['fileSize'] = `File size is too large ${_returnFileSize(file.size)}`;
    }

    if (!_.includes(acceptFileTypes, file.type)) {
      newErrors['fileType'] = 'Invalid File type';
    }

    setErrors(newErrors);
    return (
      !newErrors['fileSize'] &&
      !newErrors['fileType']
    );
  };

  const _uploadFile = () => uploadFile({ file, signedUrl, documentId, isPublic });

  // ******************** listeners ********************
  const _dropListener = (e: React.DragEvent<HTMLDivElement>) => {
    _overrideEventDefaults(e);

    const files = e.dataTransfer.files;
    if (files && files[0]) {
      if (_validate(files[0])) {
        startSpinner && startSpinner();
        _getSignedUrl(files[0]);
      }
    }

    setDragEventCounter(0);
    setDragging(false);
  };

  const _onFileChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files && files[0]) {
      if (_validate(files[0])) {
        startSpinner && startSpinner();
        _getSignedUrl(files[0]);
      }
    }
  };

  const _onSelectFileClick = () => {
    if (dropzone.current) {
      // @ts-ignore
      dropzone.current.click();
    }
  };

  const _dragEnterListener = (e: React.DragEvent<HTMLDivElement>) => {
    const items = e.dataTransfer.items;
    const types = e.dataTransfer.types;

    _overrideEventDefaults(e);
    const dragging = dragEventCounter + 1;
    if (items && items[0]) {
      setDragEventCounter(dragging);
      setDragging(true);

    } else if (types && types[0] === 'Files') {
      // support for IE
      setDragEventCounter(dragging);
      setDragging(true);
    }
  };

  const _dragLeaveListener = (e: React.DragEvent<HTMLDivElement>) => {
    _overrideEventDefaults(e);
    const dragging = dragEventCounter - 1;

    if (dragging === 0) {
      setDragging(false);
    }

    setDragEventCounter(dragging);
  };
  // ***************************************************

  useEffect(() => {
    if (dropzone.current) {
      // @ts-ignore
      dropzone.current.addEventListener('dragover', (e: Event) => _overrideEventDefaults(e));
      // @ts-ignore
      dropzone.current.addEventListener('drop', (e: Event) => _overrideEventDefaults(e));
    }

    return () => {
      window.removeEventListener('dragover', _overrideEventDefaults);
      window.removeEventListener('drop', _overrideEventDefaults);
    };
  }, [dropzone]);

  const dropZone = (
    <DropZone
      onDrag={_overrideEventDefaults}
      onDragStart={_overrideEventDefaults}
      onDragEnd={_overrideEventDefaults}
      onDragOver={_overrideEventDefaults}
      onDragEnter={_dragEnterListener}
      onDragLeave={_dragLeaveListener}
      onDrop={_dropListener}
      dragging={dragging}
    >
      <Contents>
        <FileName>{file ? file.name : 'No File Uploaded'}</FileName>
        <div>Drag & Drop File</div>
        <div>or</div>
        <div onClick={_onSelectFileClick}>
          Select File
        </div>
        {maxFileSizeKB && <Blue>Max File Size: {fileSize}{measure}</Blue>}
        {_.map(errors, (error, key) => (
          <ErrorMessage key={key}>{error}</ErrorMessage>
        ))}
        <input
          ref={dropzone}
          type="file"
          accept={`${acceptFileTypes.join(',')}`}
          multiple={false}
          onChange={_onFileChanged}
          style={{ margin: '0 auto' }}
        />
      </Contents>
    </DropZone>
  );

  return (
    <Container>
      {isChild ? dropZone : (
        <Dialog open={isOpen} >
          <DialogTitle>File Uploader</DialogTitle>
          <DialogContent>
            {message && <MessageContainer>{message}</MessageContainer>}
            {dropZone}
          </DialogContent>
          <DialogActions>
            <Button
              buttonType="flat"
              variant="flat"
              label={isSpinning ? <CircularProgress size={24} thickness={3} style={{ color: BLUE }} /> : 'Add'}
              onClick={_uploadFile}
              style={{ color: BLUE }}
              disabled={!file || isSpinning}
            />
            <Button
              buttonType="flat"
              variant="flat"
              label="Cancel"
              onClick={_hideFileUploader}
              style={{ color: GREY }}
            />
          </DialogActions>
        </Dialog>
      )}
    </Container>
  );
};

interface FileUploaderProps {
  isOpen?: boolean;
  subBucket: string;
  message?: string | ReactElement;
  isChild?: boolean;
  acceptFileTypes?: string[];
  maxFileSizeKB?: number;
  callback?: (file: File, signedUrl: string) => void;
  uploadFile: (params: FileUploadParams) => void;
  hideFileUploader: () => void;
  isPublic?: boolean;
  isSpinning?: boolean;
  startSpinner?: () => void;
  stopSpinner?: () => void;
  hideAlertBox?: () => void;
  showErrorMessage?: (error: string | ReactElement, any) => void;
}

interface Error {
  fileSize: string;
  fileType: string;
}

export interface FileUploadParams {
  file?: File;
  signedUrl: string;
  documentId: string;
  isPublic: boolean;
}

export default FileUploader;