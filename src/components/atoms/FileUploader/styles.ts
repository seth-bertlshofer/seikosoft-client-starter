import styled from 'styled-components';
import { BLUE, LIGHT_BLUE, RED, WHITE } from '../../../styles/colors';

export const Container = styled.div `

`;
export const DropZone = styled.div<{ dragging?: boolean }> `
  width: 400px;
  border: 2px dashed ${BLUE};
  border-radius: 10px;
  padding: 20px;
  font-size: 16px;
  background-color: ${props => props.dragging ? LIGHT_BLUE : WHITE};
`;
export const Contents = styled.div `
  height: 80%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
export const FileName = styled.span `
  font-weight: 700;
`;
export const Blue = styled.div `
  color: ${BLUE};
`;
export const ErrorMessage = styled.div `
  color: ${RED};
`;
export const MessageContainer = styled.div `
  width: 400px;
  text-align: center;
`;