import React, { useState, useEffect, useCallback } from 'react';
import styled from 'styled-components';
import zxcvbn from 'zxcvbn';
import { LinearProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { BLUE, RED, YELLOW, GREEN } from '../../styles/colors';

const PasswordStrengthMeter = ({ password }) => {
  const [completed, setCompleted] = useState(0);
  const [color, setColor] = useState(RED);
  const [label, setLabel] = useState('Weak');

  const _updateStrength = useCallback(() => {
    const result = zxcvbn(password);
    switch (result.score) {
      case 0:
        setLabel('Weak');
        setColor(RED);
        setCompleted(0);
        break;

      case 1:
        setLabel('Weak');
        setColor(RED);
        setCompleted(25);
        break;

      case 2:
        setLabel('Fair');
        setColor(YELLOW);
        setCompleted(50);
        break;

      case 3:
        setLabel('Good');
        setColor(BLUE);
        setCompleted(75);
        break;

      case 4:
        setLabel('Strong');
        setColor(GREEN);
        setCompleted(100);
        break;
    }
  }, [password]);

  useEffect(() => {
    _updateStrength();
  }, [_updateStrength]);

  const ColorLinearProgress = withStyles({
    barColorPrimary: {
      backgroundColor: color,
    },
  })(LinearProgress);

  return (
    <Container>
      <ColorLinearProgress variant="determinate" value={completed} />
      <br />
      <Label>Password strength: {label}</Label>
    </Container>
  );
};

const Container = styled.div `
`;
const Label = styled.div `
`;

export default PasswordStrengthMeter;