import React  from 'react';
import InputMask from 'react-input-mask';
import { FormControl, FormHelperText, TextField } from '@material-ui/core';

const MaskedInputBox = ({
  label,
  value,
  type,
  inputStyle,
  style,
  placeholder,
  autoFocus,
  disabled,
  fullWidth,
  required,
  errorMessage,
  onChange,
  onBlur,
  onFocus,
  onPaste,
  inputMask,
}: MaskedInputBoxProps) => {
  const _onChange = e => onChange(e.target.value);
  const _onBlur = e => onBlur ? onBlur(e.target.value) : {};
  const _onFocus = e => onFocus ? onFocus(e.target.value) : {};
  const _onPaste = e => onPaste ? onPaste(e) : {};

  const hasError = errorMessage ? errorMessage !== '' : false;

  return (
    <FormControl
      disabled={ disabled }
      fullWidth={ fullWidth }
      error={hasError}
      margin="normal"
      required={ required }
      style={ style }
    >
      <InputMask
        mask={inputMask}
        value={value}
        onChange={_onChange}
        onBlur={_onBlur}
        onFocus={_onFocus}
        onPaste={_onPaste}
        disabled={disabled}
      >
        {() => (
          <TextField
            margin="normal"
            label={label}
            type={type}
            placeholder={placeholder}
            autoFocus={autoFocus}
            disabled={disabled}
            error={hasError}
            style={{ ...inputStyle, height: (inputStyle && inputStyle.height) || '2.188em' }}
          />
        )}
      </InputMask>
      {hasError && <FormHelperText error={hasError}>{ errorMessage }</FormHelperText>}
    </FormControl>
  );
};

interface MaskedInputBoxProps {
  inputMask: any;
  value: string | number | undefined | null;
  onChange: (value: string | number) => void;
  label?: string;
  inputStyle?: any;
  style?: Record<string, unknown>;
  placeholder?: string;
  type?: string;
  onBlur?: (value: string | number) => void;
  onFocus?: (value: string | number) => void;
  onPaste?: (value: string | number) => void;
  autoFocus?: boolean;
  disabled?: boolean;
  fullWidth?: boolean;
  required?: boolean;
  errorMessage?: string;
}

export default MaskedInputBox;
