import React          from 'react';
import { Editor, EditorState }  from 'react-draft-wysiwyg';
import styled         from 'styled-components';
import ImageUploader  from './ImageUploader';
import { RED, LIGHTER_GREY, GREY } from '../../styles/colors';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const WysiwygEditor = ({ height, label, editorState, toolbar, imageUploader, update, disabled, error }: WysiwygEditorProps) => (
  <Container height={height}>
    {label && <label>{label}</label>}
    <EditorWrapper>
      {imageUploader && <ImageUploader
        acceptedTypes={[
          'image/gif',
          'image/jpeg',
          'image/png',
          'image/svg+xml', // svg
        ]}
        uploadFile={update}
      />}
      <Editor
        editorState={editorState}
        wrapperClassName="wrapper"
        toolbarClassName="toolbar"
        editorClassName={`editor ${error ? 'validation-error' : ''}`}
        onEditorStateChange={update}
        toolbar={toolbar}
        readOnly={disabled}
      />
      {error && <Alert>{error}</Alert>}
    </EditorWrapper>
  </Container>
);

const Container = styled.div<{ height?: number; }> `
  position: relative;
  height: ${props => props.height || 500}px;
  margin-bottom: 25px;
  margin-left: 6px;
  .wrapper {
    height: 80%;
  }
  .toolbar {
    background-color: ${LIGHTER_GREY};
  }
  .editor {
    border: 1px solid ${GREY};
    height: 400px;
    overflow-y: auto;
    padding: 5px;
    &.validation-error {
      border: 1px solid ${RED};
    }
  }
`;
export const EditorWrapper = styled.div `
  position: relative;
`;
export const Alert = styled.div `
  color: ${RED};
`;

interface WysiwygEditorProps {
  height?: number;
  label: string;
  toolbar: any;
  mergeFields?: boolean;
  imageUploader?: boolean;
  editorState: EditorState;
  disabled?: boolean;
  error: string;
  update: (e: any) => void;
}

export default WysiwygEditor;
