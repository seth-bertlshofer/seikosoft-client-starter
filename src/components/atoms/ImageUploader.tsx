import React, { useState } from 'react';
import styled from 'styled-components';
import { Dialog, DialogActions, DialogContent, CircularProgress } from '@material-ui/core';
import WallpaperIcon from '@material-ui/icons/Wallpaper';
import HeightIcon from '@material-ui/icons/Height';

import FileUploader, { FileUploadParams } from './FileUploader/FileUploader';
import InputBox from '../atoms/InputBox';
import Button from '../atoms/Button';

import { BLUE, RED, WHITE, BLACK, GREY } from '../../styles/colors';
import { UploadParams } from '../../@types/document';

const ImageUploader = ({ acceptedTypes = ['image/*'], uploadFile, isSpinning }: ImageUploaderProps) => {

  const [isOpen, setIsOpen] = useState(false);
  const [height, setHeight] = useState('auto');
  const [width, setWidth] = useState('auto');
  const [file, setFile] = useState<File>();
  const [signedUrlImage, setSignedUrlImage] = useState('');
  const [documentIdImage, setDocumentIdImage] = useState('');

  const _toggleDropzone = () => setIsOpen(!isOpen);
  const _hideDropzone = () => setIsOpen(false);

  const _updateHeight = (key: string) => (e: any) => setHeight(e.target.value);
  const _updateWidth = (key: string) => (e: any) => setWidth(e.target.value);

  const _addImageToEditor = (file: File, data: any) => {
    setFile(file);
    setSignedUrlImage(data.signedUrl);
    setDocumentIdImage(data.documentId);
  };

  const _addImage = ({ file, signedUrl, documentId, isPublic }: FileUploadParams) =>
    uploadFile({
      file,
      signedUrl: signedUrl ?? signedUrlImage,
      documentId: documentId ?? documentIdImage,
      isPublic: isPublic ?? false,
      saveFile: _addImageToEditor
    });

  return (
    <Container>
      <ImageIcon onClick={_toggleDropzone}>
        <WallpaperIcon height={35} width={35} />
      </ImageIcon>
      <Dialog
        open={isOpen}
        onBackdropClick={_hideDropzone}
      >
        <DialogContent>
          <FileUploader
            subBucket="solicitation"
            acceptFileTypes={acceptedTypes}
            callback={_addImageToEditor}
            hideFileUploader={_hideDropzone}
            uploadFile={_addImage}
            isChild
            isPublic
          />
          <div>
            <InputBoxWrapper>
              <IconWrapper>
                <HeightIcon/>
              </IconWrapper>
              <InputBox
                label=""
                value={height}
                onChange={_updateHeight('')}
                inputStyle={{ width: '145px' }}
                fullWidth
              />
            </InputBoxWrapper>
            <InputBoxWrapper>
              <IconWrapper>
                <HeightIcon style={{ transform: 'rotate(90deg)' }} />
              </IconWrapper>
              <InputBox
                label=""
                value={width}
                onChange={_updateWidth('')}
                inputStyle={{ width: '145px' }}
                fullWidth
              />
            </InputBoxWrapper>
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            buttonType="flat"
            variant="flat"
            label={isSpinning ? <CircularProgress size={24} thickness={3} style={{ color: BLUE }} /> : 'Add'}
            style={{ color: !file ? GREY : BLUE }}
            onClick={_addImage}
            disabled={isSpinning || !file}
          />
          <Button
            buttonType="flat"
            variant="flat"
            label="Cancel"
            style={{ color: RED }}
            onClick={_hideDropzone}
          />
        </DialogActions>
      </Dialog>
    </Container>
  );
};

const Container = styled.div `
  position: absolute;
  top: 48px;
  right: 180px;
  color: ${BLACK};
  width: 35px;
  height: 30px;
  background-color: ${WHITE};
  padding: 3px 6px;
`;
const ImageIcon = styled.div `
  cursor: pointer;
`;
const InputBoxWrapper = styled.div `
  display: inline-flex;
  width: 50%;
`;
const IconWrapper = styled.div `
  margin-top: 21px;
  margin-right: 10px;
`;

interface ImageUploaderProps {
  acceptedTypes?: string[];
  uploadFile: (params: UploadParams) => void;
  isSpinning?: boolean;
}

export default ImageUploader;