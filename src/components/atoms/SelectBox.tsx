import React from 'react';
import { FormControl, FormHelperText, InputLabel, MenuItem, Select }  from '@material-ui/core';

const SelectBox = ({
  label,
  name,
  variant,
  value,
  onChange,
  disabled,
  required,
  native,
  errorMessage,
  inputStyle,
  children,
  multiple,
  hasBlankValue,
}: SelectBoxProps) => {
  const hasError = errorMessage ? errorMessage !== '' : false;
  return (
    <FormControl
      required={required}
      disabled={disabled}
      error={hasError}
    >
      <InputLabel
        id={`${name}-label`}
        style={inputStyle}
      >
        {label}
      </InputLabel>
      <Select
        labelId={`${name}-label`}
        id={name}
        value={value || ''}
        variant={variant || 'standard'}
        onChange={e => onChange ? onChange(e.target.value) : {} }
        inputProps={{
          name,
          id: name
        }}
        style={inputStyle}
        native={native}
        multiple={multiple}
      >
        {hasBlankValue && <MenuItem value={undefined}>-</MenuItem>}
        {children}
      </Select>
      {hasError && <FormHelperText error={hasError}>{errorMessage}</FormHelperText>}
    </FormControl>
  );
};

interface SelectBoxProps {
  label: string;
  name: string;
  variant?: any;
  value?: any; // string | Jurisdiction | Country | PaymentType | VoidReason | SuspendReason | AuditReason | AuditResult;
  onChange: (value: any) => void;
  children: any;
  disabled?: boolean;
  required?: boolean;
  native?: boolean;
  errorMessage?: string;
  inputStyle?: any;
  multiple?: boolean;
  hasBlankValue?: boolean;
}

export default SelectBox;