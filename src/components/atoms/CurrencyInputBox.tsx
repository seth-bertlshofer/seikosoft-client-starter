import React        from 'react';
import NumberFormat from 'react-number-format';
import { FormControl, FormHelperText, TextField } from '@material-ui/core';

const CurrencyInputBox = ({
  label,
  value,
  inputStyle,
  style,
  placeholder,
  defaultValue,
  autoFocus,
  disabled,
  fullWidth,
  required,
  errorMessage,
  onChange,
  onBlur,
  onFocus,
  onPaste,
  inputProps,
}: CurrencyInputBoxProps) => {
  const NumberFormatCustom = ({ inputRef, onChange, ...inputProps }) => (
    <NumberFormat
      {...inputProps}
      getInputRef={inputRef}
      onValueChange={values => onChange({
        target: {
          value: values.value,
        },
      })}
      thousandSeparator
      prefix="$ "
    />
  );

  const _onChange = e => onChange(e.target.value);
  const _onBlur = e => onBlur ? onBlur(e.target.value) : {};
  const _onFocus = e => onFocus ? onFocus(e.target.value) : {};
  const _onPaste = e => onPaste ? onPaste(e) : {};

  const hasError = errorMessage ? errorMessage !== '' : false;

  return (
    <FormControl
      disabled={ disabled }
      fullWidth={ fullWidth }
      error={hasError}
      margin="normal"
      required={ required }
      style={ style }
    >
      <TextField
        label={label}
        value={value}
        placeholder={placeholder}
        defaultValue={defaultValue}
        onChange={_onChange}
        onBlur={_onBlur}
        onFocus={_onFocus}
        onPaste={_onPaste}
        autoFocus={autoFocus}
        disabled={disabled}
        style={inputStyle}
        InputProps={{
          ...inputProps,
          inputComponent: NumberFormatCustom,
        }}
        error={hasError}
      />
      {hasError && <FormHelperText error={hasError}>{errorMessage}</FormHelperText>}
    </FormControl>
  );
};

interface CurrencyInputBoxProps {
  value: number;
  onChange: (value: number) => void;
  label?: string;
  name?: string;
  inputProps?: any;
  inputStyle?: any;
  style?: Record<string, unknown>;
  placeholder?: string;
  defaultValue?: any;
  onBlur?: (value: string | number) => void;
  onFocus?: (value: string | number) => void;
  onPaste?: (value: string | number) => void;
  autoFocus?: boolean;
  disabled?: boolean;
  fullWidth?: boolean;
  required?: boolean;
  errorMessage?: string;
}

export default CurrencyInputBox;
