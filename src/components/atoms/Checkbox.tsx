import React, { ReactElement } from 'react';
import CheckIcon from '@material-ui/icons/Check';
import { GREEN } from '../../styles/colors';
import { Checkbox, FormControlLabel, FormHelperText } from '@material-ui/core';

const MyCheckbox = ({ label, group, checked, disabled, errorMessage, onChange }: MyCheckboxProps) => (
  <div>
    <FormControlLabel
      control={
        <Checkbox
          name={group}
          checkedIcon={<CheckIcon style={{ color: GREEN }} />}
          checked={(checked ?? false)}
          onChange={e => onChange ? onChange(e.target.checked) : {} }
          disabled={disabled}
        />
      }
      label={label}
    />
    {errorMessage && <FormHelperText error={errorMessage !== ''}>{errorMessage}</FormHelperText>}
  </div>
);

interface MyCheckboxProps {
  label: string | ReactElement;
  group: string;
  checked: boolean | undefined | null;
  disabled?: boolean;
  errorMessage?: string;
  onChange: (value: boolean) => any;
}

export default MyCheckbox;