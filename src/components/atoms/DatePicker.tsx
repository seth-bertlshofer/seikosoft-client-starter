import React from 'react';
import { FormControl, FormHelperText }  from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { Moment } from 'moment';

const DatePicker = ({
  label,
  format,
  value,
  onChange,
  disabled,
  required,
  errorMessage,
  style,
  inputStyle,
}: DatePickerProps) => {
  const hasError = errorMessage ? errorMessage !== '' : false;
  return (
    <FormControl
      margin="normal"
      required={required}
      disabled={disabled}
      error={hasError}
      style={style}
    >
      <KeyboardDatePicker
        disableToolbar
        variant="inline"
        format={format}
        margin="normal"
        label={label}
        value={value}
        onChange={value => onChange(value)}
        KeyboardButtonProps={{
          'aria-label': 'change date'
        }}
        style={inputStyle}
        error={hasError}
      />
      {hasError && <FormHelperText error={hasError}>{errorMessage}</FormHelperText>}
    </FormControl>
  );
};

interface DatePickerProps {
  label: string;
  format: string;
  value?: Moment | null;
  onChange: (value: any) => void;
  disabled?: boolean;
  required?: boolean;
  errorMessage?: string;
  style?: any;
  inputStyle?: any;
}

export default DatePicker;