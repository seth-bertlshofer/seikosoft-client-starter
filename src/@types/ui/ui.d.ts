import AlertBoxType from '../alertBox';
import { NavRouteModel } from '../../@types/ui/navRouteModel';

export interface UiState {
  height: number;
  width: number;
  scrollPercentage: number;
  sideNavOpen: boolean;
  sideNavRoutes?: NavRouteModel[];
  isSpinning: boolean;
  alertBox?: AlertBoxType;
  isBtnDisabled: boolean;
  showAlertBanner: boolean;
}