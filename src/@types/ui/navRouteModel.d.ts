import { SubRouteModel } from './subRouteModel';

export interface NavRouteModel {
  Route: string;
  Icon: string;
  Label: string;
  SubRoutes?: SubRouteModel[]
}

export interface ExternalLinkModel {
  title: string;
  url: string;
}