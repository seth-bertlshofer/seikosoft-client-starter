export interface UploadParams {
  file: File | undefined,
  signedUrl: string,
  documentId: string,
  isPublic: boolean,
  saveFile: ((file: File, signedUrl: string, documentId: string) => void) | undefined;
}