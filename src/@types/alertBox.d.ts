import { ReactElement } from 'react';

interface AlertBoxType {
  title: string | ReactElement | any;
  actions?: AlertBoxActions;
  disableBackdropClick?: boolean;
  fullScreen?: boolean;
  reason?: string;
  message: string | any;
}

export interface AlertBoxActions {
  [key: string]: AlertBoxAction;
}

export interface AlertBoxAction {
  color?: string;
  styles?: any;
  onClick: () => void;
}

export default AlertBoxType;