interface String {
  currency(): string;
  formatNumber(): string;
  decimal(): string;
  cardDate(): string;
  state(): string;
  phoneNumber(): string;
}

interface Number {
  currency(): string;
  formatNumber(): string;
}

declare module '*.png' {
  const value: any;
  export default value;
}

interface Window {
  _env: any;
}
