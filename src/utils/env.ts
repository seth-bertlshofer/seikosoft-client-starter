const env = (envVarName: string): string => {
  if (window._env) {
    return window._env[envVarName] || process.env[envVarName];
  }

  return process.env[envVarName] ?? '';
};

export default env;