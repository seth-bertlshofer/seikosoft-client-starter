// https://github.com/axios/axios#axios-api
import axios from 'axios';

const instance = axios.create({
  baseURL: '/api/',
  headers: {
    'Cache-Control': 'no-cache,no-store,must-revalidate,max-age=0,private'
  }
});

instance.interceptors.request.use(
  async config => {
    // TODO: configure how to get the JWT for auth calls
    const token = '';
    if (token) {
      config.headers = {
        'Cache-Control': 'no-cache,no-store,must-revalidate,max-age=0,private',
        'Authorization': `Bearer ${token}`
      };
    }

    return config;
  },
  error => Promise.reject(error)
);

instance.interceptors.response.use(
  async response => {
    // Note: here you can do things with successful request before
    // they get returned to who called them

    return response;
  },
  async error => {
    // Note: here you can do things with error responses before
    // they get returned to who called them e.g. show an alertbox
    // with the error message that gets returned

    return Promise.reject(error);
  }
);

export default instance;