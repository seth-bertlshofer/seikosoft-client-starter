
import _ from 'lodash';

export const validateEmail = email => {
  const reEmail = /^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

  let isValid = true;
  if (!email) {
    isValid = false;
  }

  if (email.length > 254) {
    isValid = false;
  }

  if (!reEmail.test(email)) {
    isValid = false;
  }

  // Further checking of some things regex can't handle
  const parts = email.split('@');

  if (_.size(parts) !== 2) {
    return false;
  }

  if (parts[0].length > 64) {
    isValid = false;
  }

  const domainParts = parts[1].split('.');
  if (domainParts.some(part => { return part.length > 63; })) {
    isValid = false;
  }

  return isValid;
};