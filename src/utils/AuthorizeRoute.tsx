import React, { useCallback, useEffect } from 'react';
import { Route } from 'react-router-dom';

const AuthorizeRoute = ({ path, Component, ...rest }) => {

  const _isAuthenticated = useCallback(async () => {
    // TODO: authorization logic
  }, []);

  useEffect(() => {
    _isAuthenticated();
  }, [_isAuthenticated]);

  return <Route { ...rest } render={props => <Component {...props} />} />;
};

export default AuthorizeRoute;