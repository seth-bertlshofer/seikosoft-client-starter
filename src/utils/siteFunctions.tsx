import React from 'react';
import _ from 'lodash';

export const getErrorMessage = (error: any): any => {
  let message;
  if (error.response) {
    if (error.response.data.message) {
      message = `${error.response.data.message}${error.response.data.requestId ? `\nError Code: ${error.response.data.requestId}` : ''}`;
    } else if (error.response.data.errors && _.size(error.response.data.errors) > 0) {
      message = _.map(error.response.data.errors, (error, key) => (
        <div key={key}>{error}</div>
      ));
    } else if (error.response.statusText) {
      message = error.response.statusText;
    }
  } else if (error.errors) {
    message = _.map(error.errors , (error, key) => (
      <div key={key}>{error}</div>
    ));
  } else {
    message = error.message ? error.message : error;
  }

  return message;
};

export const routeTo = (path: string, newTab?: boolean) => {
  if (newTab) {
    window.open(path, '_blank', 'noopener noreferrer');
  } else {
    window.location.pathname = path;
  }
};

// This is used to check whether a user has a specific permission on the "Role" type
// export const hasPermission = (permissions: Role[] | undefined, permission: Role): boolean => {
//   if (permissions !== undefined) {
//     return _.findIndex(permissions, i => (i === permission)) !== -1
//   }

//   return false
// }

export const isNull = (value): boolean => {
  if (value === '' || value === undefined || value === null || isNaN(value)) {
    return true;
  }
  return false;
};

export const getFileName = headers => {
  const fileRe = /filename=(.*);/;
  const fileNameHeader = headers['content-disposition'];

  const match = fileRe.exec(fileNameHeader);
  if (match && match[1]) {
    return match[1];
  }

  return null;
};

export const getCookie = name => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);

  if (_.size(parts) > 0) {
    return parts.pop()?.split(';').shift();
  }
};

export const getDocHeight = (): number => Math.max(
  document.body.scrollHeight, document.documentElement.scrollHeight,
  document.body.offsetHeight, document.documentElement.offsetHeight,
  document.body.clientHeight, document.documentElement.clientHeight
);
