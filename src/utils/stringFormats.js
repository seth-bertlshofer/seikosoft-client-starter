/* eslint-disable */
import { PhoneNumberUtil, PhoneNumberFormat } from 'google-libphonenumber'

String.prototype.currency = function() {
  let num = this
  try {
    num = num.decimal()
    const cents = num.match(/\.(\d{2})/g)

    num = num === '' ? '0' : num
    const numTemp = parseFloat(num.valueOf())

    const sign = (parseFloat(num.valueOf()) === Math.abs(numTemp)) ? '' : '-'

    num = `${Math.floor(numTemp * 100 + 0.50000000001)}`
    num = Math.floor(parseFloat(num.valueOf()) / 100).toString()

    for (let i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
      num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3))
    }

    num = num.replace('-','')

    return `${sign} $${num}${cents || '.00'}`
  } catch (e) {
    return num.valueOf()
  }
}

String.prototype.formatNumber = function() {
  let num = this
  try {
    num = num.decimal()

    num = num === '' ? '0' : num
    const numTemp = parseFloat(num.valueOf())

    const sign = (parseFloat(num.valueOf()) === Math.abs(numTemp)) ? '' : '-'

    num = `${Math.floor(numTemp * 100 + 0.50000000001)}`
    num = Math.floor(parseFloat(num.valueOf()) / 100).toString()

    for (let i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
      num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3))
    }

    num = num.replace('-','')

    return `${sign} ${num}`
  } catch (e) {
    return num.valueOf()
  }
}

Number.prototype.currency = function() {
  const num = this
  try {
    return num.toFixed(2).currency()
  } catch (e) {
    return num.valueOf()
  }
}

Number.prototype.formatNumber = function() {
  const num = this
  try {
    return num.toFixed(0).formatNumber()
  } catch (e) {
    return num.valueOf()
  }
}

String.prototype.decimal = function() {
  const currency = this
  try {
    return parseFloat(currency.replace(/\$|,|\s/g, '')).toFixed(2).toString()
  } catch (e) {
    return currency.valueOf()
  }
}

String.prototype.cardDate = function () {
  let date = this
  try {
    date = date.replace('/', '')
    const match = date.match(/.{1,2}/g)
    if (match && match[0].length === 2) {
      if (match[1]) {
        return `${match[0]}/${match[1]}`

      } else {
        return match[0].toString()
      }

    } else if (match) {
      return match[0].toString()
    } else {
      return date.valueOf()
    }
  } catch (e) {
    return date.valueOf()
  }
}

String.prototype.state = function () {
  const text = this
  try {
    return text.slice(-2).toUpperCase()
  } catch (e) {
    return text.valueOf()
  }
}

String.prototype.phoneNumber = function() {
  const phoneNumber = this
  const phoneUtil = PhoneNumberUtil.getInstance()

  try {
    const phoneNumberTemp = phoneUtil.parse(phoneNumber.valueOf(), 'US')
    return phoneUtil.format(phoneNumberTemp, PhoneNumberFormat.NATIONAL).toString()
  } catch (e) {
    return phoneNumber.valueOf()
  }
}