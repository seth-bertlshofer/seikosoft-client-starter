import React, { useState, useEffect } from 'react';
import env from './env';

const Timer = ({ seconds, callback }: TimerProps) => {
  const [elapsedTime, setTime] = useState(0);
  const [hasCalled, setHasCalled] = useState(false);

  let showCounter = false;
  switch(env('REACT_APP_ENV')) {
    case 'development':
      showCounter = true;
      break;
  }

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(time => time + 1);
    }, 1000);

    if (elapsedTime === seconds && !hasCalled) {
      callback();
      setHasCalled(true);
    }

    return () => clearInterval(interval);
  }, [elapsedTime, seconds, callback, hasCalled]);

  return (<div style={{ position: 'absolute', bottom: 0, right: 10 }}>{showCounter && elapsedTime}</div>);
};

interface TimerProps {
  seconds: number;
  callback: () => void;
}

export default Timer;