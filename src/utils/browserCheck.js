// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

export const browserCheck = () => {
  const supportedBrowser = (
    // desktop browsers
    navigator.userAgent.match(/Chrome/i) ||
    navigator.userAgent.match(/Safari/i) ||
    navigator.userAgent.match(/Firefox/i) ||
    navigator.userAgent.match(/Edge/i)
  );

  const unsupportedBrowser = (
    navigator.userAgent.match(/Trident/i) // IE
  );

  if (unsupportedBrowser) {
    return 'unsupported';

  } else if (!supportedBrowser) {
    return 'unknown';
  }

  return 'supported';
};
