import React from 'react';

const ProtectedHome = () => {
  return (
    <div>
      You are on the protected home screen
    </div>
  );
};

export default ProtectedHome;