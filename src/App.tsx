import React, { useEffect, useCallback, Suspense, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Switch, Route }  from 'react-router-dom';
import { CircularProgress, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import _ from 'lodash';
import styled from 'styled-components';

import Layout from './components/Layout';
import Button from './components/atoms/Button';
import AuthorizeRoute from './utils/AuthorizeRoute';

import ProtectedHome from './pages/ProtectedHome';
import { Home } from '@material-ui/icons';

import Timer        from './utils/timer';
import useDebounce  from './utils/customHooks/useDebounce';
import usePrevious  from './utils/customHooks/usePrevious';
import { getDocHeight } from './utils/siteFunctions';

import { ApplicationState } from './store';
import { setWindowDimensions, setScrollPercent } from './store/reducers/ui/actions';

import { BLUE } from './styles/colors';

const App = () => {
  const dispatch = useDispatch();

  const props = useSelector((state: ApplicationState) => ({
    sideNavOpen     : state.ui.sideNavOpen,
    windowWidth     : state.ui.width,
    windowHeight    : state.ui.height,
    showAlertBanner : state.ui.showAlertBanner,
    alertBox        : state.ui.alertBox,
    routes          : state.ui.sideNavRoutes,
  }));

  const prevWidth = usePrevious(props.windowWidth);
  const prevHieght = usePrevious(props.windowHeight);

  // ******************* authentication functions *******************
  const [showAlert, setShowAlert] = useState(false);
  const [timerActive, setTimerActive] = useState(true);

  const _activeTimer = () => {
    setTimerActive(false);
    setTimeout(() => setTimerActive(true), 100);
  };

  const _resetTimeoutClick = useCallback(() => {
    setShowAlert(false);
    _activeTimer();
  }, []);

  const _resetTimeout = useCallback(() => {
    _activeTimer();
  }, []);

  const _login = () => console.log('login');
  const _logout = () => console.log('logout');

  const _toggleLogoutMessage = () => setShowAlert(!showAlert);

  const [debouncedResetTimeout] = useState(() => _.debounce(_resetTimeout, 3000));

  // ******************* window dimensions and scroll *******************
  const [trackLength, setTrackLength] = useState(0);
  const [pctScrolled, setPctScrolled] = useState(0);

  const debouncedTrackLength = useDebounce(trackLength, 100);
  const debouncedPctScrolled = useDebounce(pctScrolled, 100);
  const prevDebouncedPctScrolled = usePrevious(debouncedPctScrolled);

  const _setWindowDimensions = useCallback(() => {
    if (!_.isEqual(props.windowWidth, prevWidth) ||
        !_.isEqual(props.windowHeight, prevHieght)) {
      dispatch(setWindowDimensions());
    }

    if (!_.isEqual(props.windowHeight, prevHieght)) {
      const docheight = getDocHeight();
      setTrackLength(Math.abs(docheight - props.windowHeight));
    }
  }, [dispatch, props.windowWidth, prevWidth, props.windowHeight, prevHieght]);

  const _amountscrolled = useCallback(e => {
    e.persist();
    if (e.target) {
      const scrollTop =  e.target.scrollTop;
      const pctScrolled = Math.floor(scrollTop / debouncedTrackLength * 100);
      setPctScrolled(pctScrolled);
    }
  }, [debouncedTrackLength]);

  const _hideAlertBox = () => console.log('hide alert box');

  useEffect(() => {
    if (!props.windowHeight && !props.windowWidth) {
      dispatch(setWindowDimensions());
    }
    window.addEventListener('resize', () => _setWindowDimensions());
    window.addEventListener('click', debouncedResetTimeout);
    window.addEventListener('keypress', debouncedResetTimeout);
    window.addEventListener('mousemove', debouncedResetTimeout);

    if (!_.isEqual(debouncedPctScrolled, prevDebouncedPctScrolled)) {
      dispatch(setScrollPercent(debouncedPctScrolled));
    }

    return () => {
      window.removeEventListener('resize', () => _setWindowDimensions());
      window.removeEventListener('click', debouncedResetTimeout);
      window.removeEventListener('keypress', debouncedResetTimeout);
      window.removeEventListener('mousemove', debouncedResetTimeout);
    };
  }, [
    dispatch, _setWindowDimensions, _resetTimeout,
    debouncedPctScrolled, prevDebouncedPctScrolled, debouncedResetTimeout, props.windowHeight, props.windowWidth
  ]);

  return (
    <Layout
      routes={props.routes}
      alertBox={props.alertBox}
      windowWidth={props.windowWidth}
      windowHeight={props.windowHeight}
      showAlertBanner={props.showAlertBanner}
      user={undefined}
      login={_login}
      logout={_logout}
      hideAlertBox={_hideAlertBox}
    >
      <MainWindow isOpen={props.sideNavOpen} onScroll={_amountscrolled}>
        <Suspense fallback={<div><span>Loading...</span><CircularProgress size={24} thickness={3} /></div>}>
          <Switch>
            <AuthorizeRoute exact path='/protected-home' Component={ProtectedHome} />
            <Route exact path="/" component={Home} />
          </Switch>
        </Suspense>
      </MainWindow>
      <Dialog
        open={showAlert}
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
      >
        <DialogTitle>Alert!</DialogTitle>
        <DialogContent>
          <h3>You have been inactive for 25 minutes.  Your session will end in 5 minutes.</h3>
        </DialogContent>
        <DialogActions>
          <Button
            buttonType="flat"
            variant="flat"
            label="Ok"
            style={{ color: BLUE }}
            onClick={_resetTimeoutClick}
          />
        </DialogActions>
      </Dialog>
      {timerActive && <Timer seconds={1500} callback={_toggleLogoutMessage} />}
      {timerActive && <Timer seconds={1800} callback={_logout} />}
    </Layout>
  );
};

const MainWindow = styled.div<{ isOpen: boolean; }> `
  margin-left: ${props => props.isOpen ? 252 : 75}px;
  padding: 10px 0 0 10px;
  margin-top: 65px;
  overflow-x: auto;
  overflow-y: auto;
`;

export default App;