import 'core-js/stable';
import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import 'typeface-roboto';
import './index.css';
import App from './App';
import './utils/stringFormats';
import configureStore from './store/configureStore';
import { BLUE, RED, GREY, GREEN } from './styles/colors';
import reportWebVitals from './reportWebVitals';

const history = createBrowserHistory();
const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const rootElement = document.getElementById('root');

export const store = configureStore(history);

const theme = createMuiTheme({
  palette: {
    primary: {
      main: BLUE
    },
    secondary: {
      main: GREY
    },
    error: {
      main: RED
    },
    success: {
      main: GREEN
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <ConnectedRouter history={history} baseUrl={baseUrl}>
            <App />
          </ConnectedRouter>
        </MuiPickersUtilsProvider>
      </Provider>
    </MuiThemeProvider>
  </React.StrictMode>,
  rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
