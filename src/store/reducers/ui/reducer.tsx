import { actionTypes }  from './actions';
import { getErrorMessage } from '../../../utils/siteFunctions';
import { UiState } from '../../../@types/ui/ui';

const initialState: UiState = {
  height: 0,
  width: 0,
  scrollPercentage: 0,
  sideNavOpen: window.innerWidth > 1080,
  sideNavRoutes: undefined,
  isSpinning: false,
  alertBox: undefined,
  isBtnDisabled: false,
  showAlertBanner: false,
};

const _setWindowDimensions = (state, height, width) => ({
  ...state,
  height,
  width,
});

const _setScrollPercent = (state, scrollPercentage) => ({
  ...state,
  scrollPercentage
});

const _toggleSideNav = (state, sideNavOpen) => ({
  ...state,
  sideNavOpen
});

const _setSideNavRoutes = (state, sideNavRoutes) => ({
  ...state,
  sideNavRoutes
});

const _showAlertBox = (state, title, reason, error, actions, disableBackdropClick, fullScreen = false) => ({
  ...state,
  alertBox: {
    title,
    reason,
    message: getErrorMessage(error),
    actions,
    disableBackdropClick,
    fullScreen,
  }
});

const _hideAlertBox = state => ({
  ...state,
  alertBox: undefined
});

const _startSpinner = state => ({
  ...state,
  isSpinning: true
});

const _stopSpinner = state => ({
  ...state,
  isSpinning: false
});

const _disableButton = state => ({
  ...state,
  isBtnDisabled: true
});

const _enableButton = state => ({
  ...state,
  isBtnDisabled: false
});

const _unsupportedBrowserBanner = (state, showAlertBanner) => ({ ...state, showAlertBanner });

export default (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.SET_WINDOW_DIMENSIONS:
      return _setWindowDimensions(state, action.height, action.width);

    case actionTypes.SET_SCROLL_PERCENT:
      return _setScrollPercent(state, action.percent);

    case actionTypes.TOGGLE_SIDE_NAV:
      return _toggleSideNav(state, action.isOpen);

    case actionTypes.SET_SIDE_NAV_ROUTES:
      return _setSideNavRoutes(state, action.routes);

    case actionTypes.SHOW_ALERT_BOX:
      return _showAlertBox(state, action.title, action.reason, action.message, action.actions, action.disableBackdropClick, action.fullScreen);

    case actionTypes.HIDE_ALERT_BOX:
      return _hideAlertBox(state);

    case actionTypes.START_SPINNER:
      return _startSpinner(state);

    case actionTypes.STOP_SPINNER:
      return _stopSpinner(state);

    case actionTypes.DISABLE_BUTTON:
      return _disableButton(state);

    case actionTypes.ENABLE_BUTTON:
      return _enableButton(state);

    case actionTypes.SHOW_ERROR_MESSAGE:
      return _showAlertBox(
        state,
        'Alert:',
        '',
        action.error,
        action.actions,
        true);

    case actionTypes.UNSUPPORTED_BROWSER_BANNER:
      return _unsupportedBrowserBanner(state, action.isShowing);

    default:
      return state;
  }
};