import React  from 'react';
import _      from 'lodash';
import API    from '../../../utils/network';
import env    from '../../../utils/env';
import AlertBoxType, { AlertBoxActions } from '../../../@types/alertBox';
import { BLUE } from '../../../styles/colors';

export const actionTypes = {
  SET_WINDOW_DIMENSIONS       : '/ss_project/ui/SET_WINDOW_DIMENSIONS',
  SET_SCROLL_PERCENT          : '/ss_project/ui/SET_SCROLL_PERCENT',
  TOGGLE_SIDE_NAV             : '/ss_project/ui/TOGGLE_SIDE_NAV',
  SET_SIDE_NAV_ROUTES         : '/ss_project/ui/SET_SIDE_NAV_ROUTES',
  SHOW_ALERT_BOX              : '/ss_project/ui/SHOW_ALERT_BOX',
  HIDE_ALERT_BOX              : '/ss_project/ui/HIDE_ALERT_BOX',
  SHOW_ERROR_MESSAGE          : '/ss_project/ui/SHOW_ERROR_MESSAGE',
  START_SPINNER               : '/ss_project/ui/START_SPINNER',
  STOP_SPINNER                : '/ss_project/ui/STOP_SPINNER',
  DISABLE_BUTTON              : '/ss_project/ui/DISABLE_BUTTON',
  ENABLE_BUTTON               : '/ss_project/ui/ENABLE_BUTTON',
  UNSUPPORTED_BROWSER_BANNER  : '/ss_project/ui/UNSUPPORTED_BROWSER_BANNER',
};

///////////////////////////////////////////
//        local calls
///////////////////////////////////////////
export const setWindowDimensions = () => ({
  type: actionTypes.SET_WINDOW_DIMENSIONS,
  height: window.innerHeight,
  width: window.innerWidth,
});

export const setScrollPercent = percent => ({
  type: actionTypes.SET_SCROLL_PERCENT,
  percent
});

export const toggleSideNav = (isOpen: boolean) => ({
  type: actionTypes.TOGGLE_SIDE_NAV,
  isOpen
});

export const showReleaseNotes = () => (
  (dispatch, getState) => {
    const { ui } = getState();

    dispatch(showAlertBox({
      title: 'Release Notes',
      message: (
        <iframe
          title="admin-version"
          width={ui.width - 50}
          height={ui.height - 150}
          src={env('REACT_APP_VERSION_URL')}
        ></iframe>
      ),
      actions: {
        Ok: {
          color: BLUE,
          styles: { marginRight: '100px' },
          onClick: () => {
            dispatch(hideAlertBox());
            localStorage.setItem('ucrInfo', JSON.stringify({ versionNumber: env('REACT_APP_ADMIN_VERSION') }));
          }
        }
      },
      fullScreen: true,
    }));
  }
);

export const checkReleaseNotes = () => {
  const versionNumber = env('REACT_APP_VERSION');
  // @ts-ignore
  const ucrStorageInfo = JSON.parse(localStorage.getItem('ucrInfo'));
  if (ucrStorageInfo === null || versionNumber !== ucrStorageInfo.versionNumber) {
    showReleaseNotes();
  }
};

export const setSideNavRoutes = routes => ({
  type: actionTypes.SET_SIDE_NAV_ROUTES,
  routes
});

export const showAlertBox = ({ title, reason, message, actions, disableBackdropClick, fullScreen }: AlertBoxType) => ({
  type: actionTypes.SHOW_ALERT_BOX,
  title,
  reason,
  message,
  actions,
  disableBackdropClick,
  fullScreen,
});

export const hideAlertBox = () => ({
  type: actionTypes.HIDE_ALERT_BOX
});

export const showErrorMessage = (error: any, actions: AlertBoxActions) => ({
  type: actionTypes.SHOW_ERROR_MESSAGE, // TODO: don't show this if the error message is "Request Aborted"
  error,
  actions,
});

export const startSpinner = () => ({
  type: actionTypes.START_SPINNER,
});

export const stopSpinner = () => ({
  type: actionTypes.STOP_SPINNER,
});

export const disableButton = () => ({
  type: actionTypes.DISABLE_BUTTON
});

export const enableButton = () => ({
  type: actionTypes.ENABLE_BUTTON
});

export const browserCheck = () =>
  dispatch => {
    dispatch({ type: actionTypes.UNSUPPORTED_BROWSER_BANNER, isShowing: false });
    const supportedBrowser = !!(
      // desktop browsers
      navigator.userAgent.match(/Chrome/i) ||
      navigator.userAgent.match(/Safari/i) ||
      navigator.userAgent.match(/Firefox/i) ||
      navigator.userAgent.match(/Edge/i)
    );

    const unsupportedBrowser = !!(
      navigator.userAgent.match(/Trident/i) // IE
    );

    if (unsupportedBrowser) {
      dispatch(unsupportedBrowserAlert());
      dispatch({ type: actionTypes.UNSUPPORTED_BROWSER_BANNER, isShowing: true });

    } else if (!supportedBrowser) {
      dispatch(browserSuggestionAlert());
    }
  };

///////////////////////////////////////////
//        API calls
///////////////////////////////////////////
export const exampleAPICALL = () => (
  dispatch =>
    API({
      method: 'POST',
      url: 'example/url/here',
    })
      .then(response => {
        console.log(response);
      })
      .catch(error => dispatch(showErrorMessage(error, { Ok: { onClick: () => {
        dispatch(hideAlertBox());
      } }})))
);

///////////////////////////////////////////
//            Helper functions
///////////////////////////////////////////
const unsupportedBrowserAlert = () =>
  dispatch =>
    dispatch(showAlertBox({
      title: 'Unsupported Browser Detected',
      message: <div>
        <p>We currently do not support your browser!</p>
        <div>Reasons:</div>
        <ul>
          <li>Browser is too old</li>
          <li>Security Concerns</li>
          <li>Browser is no longer supported by the creator company (e.g. Microsoft, Apple, Google)</li>
        </ul>
        <p>Please use a modern browser (e.g. latest versions of Chrome, Firefox, Safari, Edge).</p>
      </div>,
      actions: {
        Ok: {
          color: BLUE,
          onClick: () => dispatch(hideAlertBox())
        }
      },
    }));

const browserSuggestionAlert = () =>
  dispatch =>
    dispatch(showAlertBox({
      title: 'Browser Warning',
      message: <div>
        <p>We notice you are using a browser that has known issues.</p>
        <p>We recommend using a different browser (e.g. latest versions of Chrome, Firefox, Safari, Edge).</p>
      </div>,
      actions: {
        Ok: {
          color: BLUE,
          onClick: () => dispatch(hideAlertBox())
        }
      },
    }));