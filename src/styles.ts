import styled from 'styled-components';

export const SubTitle = styled.div `
  font-weight: 600;
`;
export const Contents = styled.div `
  padding: 10px;
`;