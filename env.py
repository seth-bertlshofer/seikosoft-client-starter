#!/usr/bin/env python3

import os

# Recreate config file
ENV_OUTPUT_FILE = "./public/env.js"


def main():
    if os.path.exists(ENV_OUTPUT_FILE):
        os.remove(ENV_OUTPUT_FILE)

    out_file = open(ENV_OUTPUT_FILE, 'w+')

    # Add assignment
    out_file.write('window._env = {\n')

    # read in env vars
    with open('./.env', 'r') as in_file:
        lines = in_file.readlines()
        for line in lines:
            line_info = line.strip()

            if not line_info:
                continue

            if line.startswith('#'):
                continue

            var_name = ''
            var_value = ''

            variable_info = line_info.split('=')

            if len(variable_info) == 0:
                continue

            var_name = variable_info[0].strip()
            var_value = variable_info[1].strip()
            var_value = var_value.strip('"')

            if not var_value:
                continue

            out_file.write(f'  {var_name}: "{var_value}",\n')

    out_file.write('}')
    out_file.close()
    in_file.close()


if __name__ == '__main__':
    main()
